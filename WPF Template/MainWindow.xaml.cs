﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro;
using MahApps.Metro.Controls;
using MahApps.Metro.Models;

namespace WPFTemplate
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private Theme theme;
        private String color;

        public MainWindow()
        {
            InitializeComponent();
        }

        public void ChangeTheme(string color, Theme theme)
        {
            this.color = color;
            this.theme = theme;
            ThemeManager.ChangeTheme(Application.Current, MahApps.Metro.ThemeManager.DefaultAccents.First(a => a.Name == color), theme);
        }

        private void ChangeTheme(object sender, SelectionChangedEventArgs e)
        {
            if (ThemeShade != null && ThemeColor != null)
            {
                string color = (ThemeColor.Items[ThemeColor.SelectedIndex] as ComboBoxItem).Content.ToString();
                Theme theme;
                if (ThemeShade.SelectedIndex == 0)
                {
                    theme = Theme.Light;
                }
                else
                {
                    theme = Theme.Dark;
                }

                ChangeTheme(color, theme);
            }
        }
    }
}
